const gulp = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const htmlmin = require('gulp-htmlmin');

gulp.task('t-sass', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('style:vendor', function () {
    return gulp
        .src(
            [
                './node_modules/bootstrap/dist/css/bootstrap.css',
                './node_modules/bootstrap/dist/css/bootstrap-grid.css',
                './node_modules/bootstrap/dist/css/bootstrap-reboot.css'
            ]
        )
        .pipe(gulp.dest('dist/css'));
});

gulp.task('t-html', function () {
    return gulp.src('src/**/*.html')
        .pipe(gulp.dest('dist'));
});
gulp.task('html:minify', () => {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
    // .pipe(livereload());
});
gulp.task('t-js', function () {
    return gulp.src('src/js/*.js')
        .pipe(gulp.dest('dist/js'));
});

// gulp.task('build', function () {
//
// })

gulp.task('build',

    gulp.series([
        't-js',
        't-sass',
    ]
    )
);