const gulp = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const htmlmin = require('gulp-htmlmin');

gulp.task('dist-html', function () {
    return gulp.src('src/**/*.html')
        .pipe(gulp.dest('dist'));
});
gulp.task('html:minify', function () {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'));
})
gulp.task('dist-css', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        // .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});
gulp.task('dist-js', function () {
    return gulp.src('src/js/*.js')
        .pipe(gulp.dest('dist/js'));
});

gulp.task('build',
    gulp.series([
        'dist-html',
        'dist-css',
        'dist-js'
    ])
)

gulp.task('watch', function () {
    return gulp
        .watch(
        'src/**/*.*',
            gulp.series(
                'dist-html',
                'dist-css',
                'dist-js'
            )
        )
});

gulp.task('default', gulp.series('dist-css'));