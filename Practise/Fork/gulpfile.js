const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const imgMin = require('gulp-imagemin');


gulp.task('htmlBuild', function () {
    return gulp.src('src/*.html')
        .pipe(gulp.dest('dist/'))
});
gulp.task('cssBuild', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 8 versions'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('dist/css'))
});
gulp.task('image:minify', function () {
    return gulp.src('src/img/**/*.*')
        .pipe(imgMin())
        .pipe(gulp.dest('dist/img-min'))
});
gulp.task('build', gulp.series (
    'htmlBuild',
    'cssBuild',
    'image:minify',
));
gulp.task('watch', function () {
    gulp.watch(
        'src/**/*.*',
        gulp.series(
            'htmlBuild',
            'cssBuild',
        )
    )
});