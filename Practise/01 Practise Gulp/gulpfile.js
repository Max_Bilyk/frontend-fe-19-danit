
let projectFolder = "dist";
let sourceFolder = "#src";

let path = {
    build: {
        html: projectFolder + "/",
        css: projectFolder + "/css/",
        js: projectFolder + "/js/",
        img: projectFolder + "/img/",
        fonts: projectFolder + "/fonts/",
    },
    src: {
        html: sourceFolder + "/*.html",
        css: sourceFolder + "/css/styles.css",
        js: sourceFolder + "/js/xhr.js",
        img: sourceFolder + "/img/**/*.{jpg,png,svg,gif,ico,webp,jpeg}",
        fonts: sourceFolder + "/fonts/*.ttf",
    },
    watch: {
        html: sourceFolder + "/**/*.html",
        css: sourceFolder + "/css/**/*.css",
        js: sourceFolder + "/js/**/*.js",
        img: sourceFolder + "/img/**/*.{jpg,png,svg,gif,ico,webp,jpeg}",
    },
    clean: "./" + projectFolder + "/"
}

let {src, dest} = require('gulp'),
    gulp = require('gulp'),
    browserSync = require('browser-sync').create();

function getBrowserReload () {
    browserSync.init({
        server: {
            baseDir: "./" + projectFolder + "/"
        },
        port: 3000,
        notify: false
    })
}

function html () {
    return src(path.src.html)
        .pipe(dest(path.build.html))
        .pipe(browserSync.stream())
}
let build = gulp.series(html)
let watch = gulp.parallel(getBrowserReload);

exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;