const delay = ms => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, ms)
    })
}
const url = 'https://jsonplaceholder.typicode.com/todos';
// function fetchTodos() {
//
//     console.log('Fetching...')
//
//     return delay(2000)
//         .then(() => {
//             return fetch(url)
//         })
//         .then(response => response.json())
// }
//
// fetchTodos()
//     .then(data => {
//         console.log('Data', data);
//     })
//     .catch(e => console.error(e))

async function asyncAjaxRequest() {
    console.log('Fetching...')
    try {
        await delay(2000);
        const response = await fetch(url);
        const data = await response.json();
        console.log('Data', data);
    }catch (e) {
        console.error(e)
    }

}
asyncAjaxRequest();