const requestURL = 'https://jsonplaceholder.typicode.com/users';

function sendRequest(method, url, body = null) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.open(method, url);
        // xhr.responseType = 'json';
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response);
                // console.error(xhr.response);
            } else {
                // console.log(JSON.parse(xhr.response));
                resolve(JSON.parse(xhr.response));
            }
        };
        xhr.onerror = () => {
            console.error(xhr.response);
        }
        xhr.send(JSON.stringify(body));
    });
}

// sendRequest('GET', requestURL)
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((err) => {
//         console.error(err);
//     })

const postBody = {
    name: 'Max',
    age: 14,
}
sendRequest('POST', requestURL, postBody)
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    })