const requestURL = 'https://jsonplaceholder.typicode.com/users';

function sendRequest(method, url, body = null) {
    const headers = {
        'Content-Type': 'application/json',
    };
    return fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: headers
    }).then(response => {
        if (response.ok) {
            // return response.text();
            return response.json();
        } else {
            return response.json()
                .then(error => {
                    throw new Error('Something is bad --->' + error)
                });
        }
    })
}

sendRequest('GET', requestURL)
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    })

// const postBody = {
//     name: 'Max',
//     age: 14,
// };
// sendRequest('POST', requestURL, postBody)
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((err) => {
//         console.error(err);
//     })