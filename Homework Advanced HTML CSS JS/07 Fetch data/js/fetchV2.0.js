const dataContainer = document.getElementById('filmDataContainer');

function sendRequest(url) {
    return fetch(url)
        .then(response => {
            return response.json()
        })
        .catch(e => {
            console.error(`Something is bad ---> ${e}`)
        });
}

sendRequest('https://swapi.dev/api/films/')
    .then(({results}) => {
        results
            .map(({title, opening_crawl, episode_id, characters}) => {
                // const character = Promise.all(characters.map(url => sendRequest(url)))
                //     .then(charactersList => {
                //         charactersList.forEach(({name}) => {
                //             const characterName = document.createElement('li');
                //             characterName.textContent = `Character name --- ${name}`;
                //             document.getElementById('film-data').appendChild(characterName);
                //         })
                //     })

                const ulFilmDataList = document.createElement('ul');
                dataContainer.appendChild(ulFilmDataList);

                ulFilmDataList.innerHTML =
                    `<li>Film: ${title}
                        <ul>
                            <li>Episode: ${episode_id}</li>
                            <li>Description: ${opening_crawl}</li>
                            ${Promise
                                .all(characters.map(url => sendRequest(url)))
                                .then(charactersList => {
                                    charactersList.forEach(({name}) => {
                                        `<li>Character: ${name}</li>`
                                    })
                                })}
                        </ul>
                    </li>`
            })
    })
    .catch((e) => {
        console.error(`Error ---> ${e}`)
    })