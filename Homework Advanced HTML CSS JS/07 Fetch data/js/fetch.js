function sendRequest(url) {
    return fetch(url)
        .then(response => {
            return response.json()
        })
        .catch(e => {
            console.error(`Something is bad ---> ${e}`)
        });
}

const filmUlList = document.createElement('ul');
document.body.append(filmUlList);

sendRequest('https://swapi.dev/api/films/')
    .then(({results}) => results)
    .then(films => {
        films
            .forEach(({episode_id, opening_crawl, title, characters}) => {
                const filmName = document.createElement('li');
                filmName.textContent = `Name of Star Wars episode --- ${title}`;
                filmUlList.appendChild(filmName);

                Promise
                    .all(characters.map(url => sendRequest(url)))
                    .then((charactersArrList) => {
                        const filmsOtherDataList = document.createElement('ul');
                        const filmDescription = document.createElement('li');
                        const episodeId = document.createElement('li');
                        filmName.appendChild(filmsOtherDataList);

                        episodeId.textContent = `Episode number --- ${episode_id}`;
                        filmDescription.textContent = `Description --- ${opening_crawl}`;

                        filmsOtherDataList.appendChild(episodeId);
                        filmsOtherDataList.appendChild(filmDescription);

                        charactersArrList.forEach(({name}) => {
                            const characterName = document.createElement('li');
                            characterName.textContent = `Character name --- ${name}`;
                            filmsOtherDataList.appendChild(characterName);

                        });
                    });
            });

    })
    .catch((error) => {
        console.error(`Something is wrong !!! ---> ${error}`)
    });