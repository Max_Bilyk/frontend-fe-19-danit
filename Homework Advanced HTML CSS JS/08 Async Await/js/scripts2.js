const idButton = document.getElementById('find-ip');
const ulDataList = document.createElement('ul');
document.body.appendChild(ulDataList);


class UserIp {
    constructor() {
    }

    static async getRequest(url) {
        const response = await fetch(url);
        return await response.json();
    }
}

class UserData {
    constructor(data) {
        for (let key in data) {
            this[key] = data[key];
        }
    }

    toString() {
        return `User data: 
                <ul> 
                    ${Object.entries(this)
            .map(([key, value]) => `<li>${key} - ${value}</li>`).join('')
        }
                </ul>`;
    }
}

idButton.onclick = async function () {
    const ip = await UserIp.getRequest('https://api.ipify.org/?format=json');
    console.log('User IP address ===>', ip);

    const data = await UserIp.getRequest('http://ip-api.com/json/217.25.198.197')
    const dataArr = [];
    dataArr.push(data)

    ulDataList.innerHTML = `<li>Loading...</li>`;

    // console.log(dataArr);
    // dataArr.map(item => {
    //     console.log(item);})
    ulDataList.innerHTML = dataArr.map(item =>
        `<li>${new UserData(item)}</li>`
    ).join('');
}