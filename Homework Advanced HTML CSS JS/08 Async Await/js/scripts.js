const idButton = document.getElementById('find-ip');

async function FigureOutById(url) {
    const response = await fetch(url);
    return response.json();
}

idButton.onclick = async function () {
    const ip = await FigureOutById('https://api.ipify.org/?format=json');
    const data = await FigureOutById('http://ip-api.com/json/217.25.198.197');
    const {timezone, country, countryCode, regionName, region} = data
    const dataArr = [timezone, country, countryCode, regionName, region];

    console.log('User ip --->', ip);

    const dataMapping = `<ul> ${dataArr.map(item => `<li>${item}</li>`)} </ul>`
    document.getElementById('ip-data').innerHTML = dataMapping.split(',').join(' ');
    // dataArr.forEach(item => {
    //     const p = document.createElement('p');
    //     p.innerHTML = item;
    //     document.body.appendChild(p)
    // })
}
