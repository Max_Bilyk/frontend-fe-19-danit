"use strict"
class Employee {
    static type = 'EMPLOYEE';

    constructor(data) {
        this.name = data.name;
        this.lastname = data.lastname
        this.age = data.age;
        this.salary = data.salary;
    };

    // set employeeName (name) {
    //     return this.name = name;
    // };
    //
    // set employeeSalary (salary) {
    //     return this.salary = salary;
    // };
    //
    // set employeeAgeInfo (newAge) {
    //     return this.age = newAge;
    // };

    get fullName () {
        return `${this.name} ${this.lastname}`
    }

    get employeeSalary () {
        return this.salary;
    };
}
const employee = new Employee({
    name: 'Max',
    lastname: 'Bilyk',
    age: 14,
    salary: 500,
});
console.log(employee);

class Programmer extends Employee {

    static type = 'PROGRAMMER';

    constructor(data) {
        super(data);

        this.lang = data.lang;
    }


    get employeeSalary() {
        super.employeeSalary;
        return this.salary * 3;
    };
}

const programmer1 = new Programmer({
    name: 'Poul',
    lastname: 'Piet',
    age: 22,
    salary: 700,
    lang: ['English, Deutsch'],
});
const programmer2 = new Programmer({
    name: 'Alan',
    lastname: 'So',
    age: 20,
    salary: 300,
    lang: ['French, Chinese'],
});
const programmer3 = new Programmer({
    name: 'Andrew',
    lastname: 'Post',
    age: 14,
    salary: 1000,
    lang: ['Korean'],
});
console.log(programmer1, programmer2, programmer3);