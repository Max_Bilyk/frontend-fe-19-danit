class Table extends DomProvider {
    constructor(col = 10, row = 10) {
        super('table');

        this.col = col;
        this.row = row;
    }

    _build() {
        // this.element
        for(let i = 0; i < this.row; i++) {
            const row = this.addRow();

            for(let j = 0; j < this.col; j++) {
                row.addCell();
            }

            this.element.appendChild(row.element);
        }

    }

    addRow() {
        return new Row();
    }
}