class DomProvider {
    constructor(el = 'div') {
        this._element = this.createElement(el);
    }

    get element() {
        return this._element;
    }

    createElement(el) {
        return document.createElement(el);
    }
}
