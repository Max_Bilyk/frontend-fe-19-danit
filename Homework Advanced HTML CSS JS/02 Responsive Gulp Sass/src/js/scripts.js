$(function () {
    function dropBoxMenuToggle() {
        $('.dropbox__menu').click(function () {
            $('.navbar__menu').toggleClass('media-navbar-menu');
            $('.dropbox__menu').toggleClass('bar-cross');
        });
    }
    dropBoxMenuToggle();
});