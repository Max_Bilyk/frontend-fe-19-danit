const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let root = document.getElementById('root');
let bookList = document.createElement('ul');
root.appendChild(bookList);
books.filter((item, index) => {
    try {
        if (!item.price) {
            throw new Error(`Price is not defined in ${index + 1} element of Book Array`);
        }
        if (!item.author) {
            throw new Error(`Author is not defined in ${index + 1} element of Book Array`)
        }
        if (!item.name) {
            throw new Error(`Name is not defined in ${index + 1} element of Book Array`)
        }
        if (item.name && item.price && item.author) {
            let bookListItem = document.createElement('li');
            bookListItem.innerHTML = `Author - ${item.author}, Name - ${item.name}, Price - ${item.price}`
            bookList.appendChild(bookListItem);
        }
    } catch (e) {
        console.error(e.message);
    }
});