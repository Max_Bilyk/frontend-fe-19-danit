// function createCalcFunc (n) {
//     return function (){
//         console.log(1000 * n)
//     }
// }
//
// const calc = createCalcFunc(42);
// calc();

// function createSomething (n) {
//     return function (num) {
//         return n + num
//     }
// }
//
// const addOne = createSomething(1);
// console.log(addOne(41));
//
// const addTen = createSomething(10);
// console.log(addTen(41));

// function urlGenerator (domain) {
//     return function (url) {
//         return `https://${url}.${domain}`
//     }
// }
//
// const comUrl = urlGenerator('com');
// console.log(comUrl('google'));
// console.log(comUrl('netflix'));
//
// const ruUrl = urlGenerator('ru');
// console.log(ruUrl('yandex'));
// console.log(ruUrl('vkontakte'));

/*
 Own Bind func
 */

function ownBind(context, fn) {
    return function (...arguments) {
        fn.apply(context, [arguments])
    }
}

function logPerson() {
    console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
}

const person1 = {name: 'Max', age: 14, job: 'Fullstack'}
const person2 = {name: 'Alex', age: 21, job: 'Frontend'}

ownBind(person1, logPerson)();
ownBind(person2, logPerson)();