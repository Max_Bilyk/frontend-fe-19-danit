// function f(x) {
//     let z = 10;
//     return function (y){
//         return x + y + z;
//     }
// }
//
// const firstFunc = f(5);
// console.log(firstFunc(6));

// function counterMaker () {
//     let counter = 0;
//
//     return function () {
//         return ++counter;
//     }
// }
// counterMaker()

function uniqueMaker() {
    let arr = [];
    return function () {
        if (arr.length === 5) {
            arr = [];
        }
        let a;
        do {
            a = Math.floor(Math.random() * 5);
        }
        while (arr.includes(a));
        arr.push(a);
        console.log(arr);
        return a;
    }
}
const func = uniqueMaker();
console.log(func());
console.log(func());
console.log(func());
console.log(func());
console.log(func());