/**
 * @desc Повертає дату в форматі ISO
 * @param {String} str - дата
 * @return {String}
 **/
export const toISOFormat = str => {
    return new Date(str).toISOString();
};
