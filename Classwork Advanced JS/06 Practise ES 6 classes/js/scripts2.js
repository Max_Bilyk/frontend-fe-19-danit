class Group {
    constructor(title, description, items = []) {
        this.title = title;
        this.description = description;
        this._items = items;
    };

    addItem (item) {
        this._items.push(item);
        return this;
    };

    /**
     * @desc delete Item
     * @param {Number} id
     * @return {boolean}
     **/

    deleteItemByID (id) {
        const index = this._items.findIndex(i => {
            return i.id === id
        });

        if (index >= 0){
            this._items.splice(index, 1);
            return true
        }
        return false;
    };

    get subtitle () {
        return `${this.title}(${this._items.length})`
    };

    toString(){
        return `${this.title}, ${this.description} -> (${this._items.length})`
    }
}

class Item {
    constructor(title) {
        this._id = ++Item.ID_INCREASER;
        this.title = title;
        this.created_at = new Date();
        this.updated_at = new Date();
    };

    get id () {
        return this._id;
    };
}
Item.ID_INCREASER = 0;

class Product extends Item{
    /**
     * @param {object} options
     *  @property {string} title
     *  @property {number} price
     **/
    constructor(options) {
        const {title, price,} = options;
        super(title);
        this.price = price
    };

    get price () {
        return `${this._price.toFixed(2)} EUR`
    };


    set price (value) {
        if (value < 0) {
            throw new Error('Price is incorrect.');
        }
        this._price = +value;
    };
}
//Groups
const group1 = new Group('TV', 'Black');
group1.addItem(new Product({title: 'LG 3310', price: 2000}))
      .addItem(new Product({title: 'Samsung Max 228', price: 1337}))
      .addItem(new Product({title: 'Apple', price: 5000}))

const group2 = new Group('PC', 'White');
group2.addItem(new Product({title: 'Hyper X', price: 10000}))
      .addItem(new Product({title: 'Lenovo 1337', price: 13377}))

alert(group1);
alert(group2);

// const product1 = new Product ({title: 'TV', price: 3000});
// const product2 = new Product ({title: 'Tablet', price: 22222});
// const product3 = new Product ({title: 'IPhone', price: 432});
// const product4 = new Product ({title: 'Laptop', price: 41324});
// const product5 = new Product ({title: 'shit', price: 1});
// console.log(product1, product2, product3, product4, product5);