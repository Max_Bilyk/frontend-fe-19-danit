// class Task {
//     constructor(title, done = false) {
//         this.title = title;
//         this._done = done;
//         this._estimation = 0;
//     }
//
//     get done() {
//         return this._done ? 'Сделано' : 'Не сделано'
//     }
//
//     set done(val) {
//         this._done = val;
//     }
//
//     set estimation(val) {
//         if (val < 0 || val > 12) {
//             throw new Error('The mark should be exactly in certain interval 0-12')
//         }else if (val < 5){
//             throw new Error('Двоечник')
//         }
//         this._estimation = val;
//     }
// }
// const obj = new Task('Сделать домашку', true);
//
// for (const field in obj){
//     console.log(`${field} ---> ${obj[field]}`)
// }
