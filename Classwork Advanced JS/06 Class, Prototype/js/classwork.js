class Warrior {
    constructor(health = 1000, power = 100) {
        this.health = health;
        this.power = power;
        this._points = 0;
    }
    addPoints (points) {
        this._points = points;
        return this
    }
    decreaseHealth(points) {
        this.health -= points;
        return this
    }

    set X(x){
        this.x = x
    }
}
class Human extends Warrior{
    constructor() {
        super(800, 100);
    }
}
class Ork extends Warrior{
    constructor() {
        super(2000, 150);
    }
}
class Elf extends Warrior{
    constructor(health, power) {
        super(500, 50);
    }
}
class WhiteElf extends Elf{
    constructor() {
        super(300, 80);
    }
}
class BlackElf extends Elf {
    constructor() {
        super(600, 90);
    }
}