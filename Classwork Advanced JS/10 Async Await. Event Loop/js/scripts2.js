async function execGetQuery (url) {
    const response = await fetch(url)

    return response.json();
}

async function getFilms() {
    return execGetQuery('https://swapi.dev/api/films/')
}

async function getCharacter(url) {
    return await execGetQuery(url);
}

getFilms()
    .then(({results}) => results)
    .then(films => {
        films.forEach(({characters}) => {
            // const promiseArr = [];
            // characters.forEach( (character) => {
            //     promiseArr.push(execGetQuery(character))
            // });
            //
            // Promise.all(promiseArr)
            //     .then(result => {})
        });
    });