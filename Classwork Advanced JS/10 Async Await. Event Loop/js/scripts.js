function getSomeData() {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(() => {
                resolve({title: 'data'})
            }, 1500);
        } catch (e) {
            reject(e)
        }
    })
}


// getSomeData()
//     .then(result => {
//         console.log('Result ->', result);
//     })

async function asyncFunction () {
    const data = await getSomeData();

    console.log('Hello my dear friend', data)

    return 'Some Value'
}

asyncFunction()
    .then(res => {
        console.log('Result from async function ->',res);
    })