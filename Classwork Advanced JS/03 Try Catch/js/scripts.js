document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('form');
    form.addEventListener('submit', formOnSubmit);
});
function formOnSubmit (e) {
    e.preventDefault();

    const {target: form} = e;
    //const form = e.target;

    const data = {};

    for (let el of form){
        data[el.name] = el.value;
        // console.log(el);
    }

    try {
        const person = new Worker(data);
        console.log(person.getFullName());
    }catch (e){
        let divError = document.getElementById('error-container');
        divError.innerHTML = e.message;
        divError.classList.toggle('active')
    }
    // const person = new Worker(data);
    // console.log(person);
}
function Worker ({name, lastName, position, weight, height}) {
    if (!name){
        throw new Error('Name is required')
    }
    if (!lastName){
        throw new Error('Last name is required')
    }
    this.name = name;
    this.lastName = lastName;
    this.position = position;
    this.weight = weight;
    this.height = height;
}
Worker.prototype.getFullName = function () {
    return `${this.name} ${this.lastName}`
}