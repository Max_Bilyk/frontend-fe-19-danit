
const p1 = {
    name: 'p1',
    age: 20,
    doSomething: function (param1, param2) {
        console.log(`Name -> ${this.name}`)
        console.log(param1 + param2)
        return true
    }
}
const p2 = {
    name: 'p2',
    age: 228,
    doSomethingForP2: function (param1) {
        console.log(`Method in p2 obj -> ${this.name} ${param1}`);
        return true;
    }
}

