function Person({name, age, height}) {
    this.person_name = name;
    this.person_height = height;
    this.person_age = age;
    this.toString = function () {
        return `${this.person_name} -> ${this.person_age}`
    };
    this.callPersonMethod = function () {
        return `Calc for Person -> ${this.person_name}`
    };
}
const person = new Person({
    name: 'Max',
    age: 14,
    height: 2.10,
})
const p1 = {
    person_name: 'Max',
    person_height: 172,
    person_age: 14,
    toString: function () {
        return`${this.person_name} -> ${this.person_age}`
    },
    doHust: function () {
        return `Do hust -> ${this.person_name}`
    }
};

//call
//apply
//bind

// alert(p1);
// alert(person)
console.log(p1.callPersonMethod().call(person));