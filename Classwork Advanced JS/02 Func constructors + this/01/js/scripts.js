// const Manager = function (name, sales){
//   this.name = name;
//   this.sales = sales;
// };
// Manager.prototype.sell = function (thing) {
//     this.sales += 1;
//     return `Manager ${this.name} sold ${thing}`
// };
//
// const John = new Manager('John', 120);
// console.log(John.sales);
// console.log(John.sell('Apple'));

function Person({name, age, height}) {
    this.person_name = name;
    this.person_height = height;
    this.person_age = age;
}
const p  = new Person({
    name: 'Max',
    age: 14,
    height: 172
});
console.log(p);

const arr = [
    new Person({
        name: 'Den',
        age: 35,
        height: 180
    }),
    new Person({
        name: 'Max',
        age: 14,
        height: 172
    }),
    new Person({
        name: 'Ala',
        age: 20,
        height: 160
    })
]-
arr.forEach(person =>{
    console.log(`${person.person_name} | ${person.person_age}`)
});
