function sum(arr) {
    return arr.reduce(function(a, b) {
        return a + b;
    });
}
console.log((sum([1, 2, 3])));

function sumArgs () {
    return Array.prototype.reduce.call(arguments, (
        function(a, b) {
            return a + b
        }, 10))
}

console.log(sumArgs(22,32))