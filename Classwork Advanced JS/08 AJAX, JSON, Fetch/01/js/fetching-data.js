document.addEventListener('DOMContentLoaded', function () {
    const button = document.getElementById('request-btn');

    button.onclick = onSendRequestBtnClick;
});

/**
 * @desc Execute HTTP request
 **/
function onSendRequestBtnClick() {
    const xml = new XMLHttpRequest();

    xml.open('GET', 'https://swapi.dev/api/people');

    xml.onreadystatechange = function () {
        try {
            if (xml.readyState === 4 && xml.status === 200) {
                console.log(' --->', JSON.parse(xml.responseText));
            }
        } catch (e) {
            console.log('Error -> ', e);
        }
    };
    xml.send();
}