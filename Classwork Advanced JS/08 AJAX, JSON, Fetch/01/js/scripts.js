const user = {
    name: 'Max',
    age: 14,
    address: {
        city: 'Kiev',
        country: 'UA',
        zip: '02152'
    },
};

try {
    localStorage.setItem('user', JSON.stringify(user));

    const str = localStorage.getItem('user');
    const parsedUser = JSON.parse(str);

    console.log(str, parsedUser);

    const badStr = '{"user" : "Anton"}';
    JSON.parse(badStr);
} catch (e) {
    console.log(e);
}

x