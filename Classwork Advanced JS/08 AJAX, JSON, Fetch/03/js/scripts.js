const requestBtn = document.getElementById('requester');
requestBtn.addEventListener('click', sendRequestOnBtnClick)
function sendRequestOnBtnClick () {
    fetch('https://swapi.dev/api/people/', {method: 'GET', })
        .then(response => {
            return response.json();
        })
        .then(response => {
            console.log(response);
        })
        .catch(e => {
           console.error(e)
        })
        .finally(() => {
            console.log('Request is completed');
        })
}

