class Ajax {
    constructor() {};

    static getRequest(url) {
        return new Promise((resolve, reject) => {
            const xml = new XMLHttpRequest();

            xml.open('GET', url);

            xml.onreadystatechange = function () {
                try {
                    if (xml.readyState === 4 && xml.status === 200) {
                        resolve(JSON.parse(xml.responseText));
                    } else if (xml.readyState === 4 && xml.status > 200) {
                        resolve(xml.responseText);
                    }
                } catch (e) {
                    reject(e);
                }
            };

            xml.onerror = reject;
            xml.onabort = reject;

            xml.send();
        });
    };
}

class Person {
    constructor(props) {

        for (let key in props) {
            this[key] = props[key];
        }
    };

    toString() {
        return ` Person: <ul> ${Object.entries(this)
            .map(([key, value]) =>
                `<li>${key}: ${value}</li>`)
            .join('')}
                        </ul>`;
    };
}

const ulPersonList = document.createElement('ul');

document.body.appendChild(ulPersonList);
ulPersonList.innerHTML = '<li>Loading...</li>';
ulPersonList.setAttribute('id', 'ul-wrapper');
ulPersonList.onclick = onPersonBtnClick;

Ajax.getRequest('https://swapi.dev/api/people/')
    .then((response) => {
        ulPersonList.innerHTML = response.results
            .map((item, index) =>
                `<li>${new Person(item)} 
                    <button>Delete</button>
                 </li>`)
            .join('');
        // console.log(response.results);
    });

function onPersonBtnClick(event) {
    const element = event.target;
    if (element.tagName === 'BUTTON') {
        element.closest("li").remove();
    }
}