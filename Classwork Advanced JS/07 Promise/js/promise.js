// let promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('NewValueFromPromise')
//     }, 1500);
// });
// console.log(promise);
// promise.then((val) => {
//     console.log(val);
//
// }).then((val2) => {
//         val2 = 'Something text';
//         console.log(val2);
// });
//
// let loadPicture = new Promise(((resolve, reject) => {
//     setTimeout(() => {
//         resolve('again new value')
//     }, 1500);
// }));

// const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         const randomNumber = Math.random() * 100;
//
//         if (randomNumber < 50) {
//             reject(new Error('VERY UNLUCKY'));
//         } else {
//             resolve({
//                 guessNumber: randomNumber
//             });
//         }
//     }, 1000);
// });
//
// promise
//     .then(function (num) {
//         console.log('YOU ARE LUCKY', num);
//     })
//     .catch(function (e) {
//         console.error(e)
//     })

function first() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve([{title: 'First Article', viewers: 123}])
        }, 2000);
    });
}

function second() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject([{title: 'Second Article', viewers: 312}])
        }, 2500);
    });
}

Promise.all([
    first(),
    second()
])
    .then((val) => {
        console.log('Nice', val)
    })
    .catch((e) => {
        console.log('Error --->', e)
    })