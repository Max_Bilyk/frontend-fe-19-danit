// const jsonFromServer = {
//     // firstName: 'Max',
//     // lastName: 'Bilyk',
//     saldo: 1000,
//     addres:{
//         country: 'UA'
//     }
// }
// // function getName(firstName, lastName) {
// //     return `${firstName} ${lastName}`
// // }
//
// function getName({firstName}) {
//     return `${firstName}`;
// }
// // console.log(getName(jsonFromServer));
// const jsonFromServer = {
//     name: 'Max',
//     number: '000031245',
//     prefix: 'SD',
//     date: '07.10.2020',
//     saldo: 20
// }
// const {number, prefix} = jsonFromServer
// function getNumPrefix ({prefix, number = 20}){
//     return `${prefix}-${number}`
// }
// console.log(getNumPrefix(jsonFromServer));
//
// function makeIndexingForSaldo (index = 1, {saldo = 20}) {
//     return saldo * index / 100;
// }
// console.log(makeIndexingForSaldo(20, jsonFromServer));
const someInfo = {
    prefix: 'DAN',
    number: 21,
    size: 10,
    name: 'Max',
    date: new Date(),
    comment: ''
}
function someFunc({prefix = 'FRE', number = 25, size = 10, ...rest}) {
    let startDate = Date.now()
    let zeroNumbers = size - String(number).length;

    const arr = [];

    for (let i = 0; i < zeroNumbers; i++){
        arr.push('0');
    }
    // console.log(arr.join(''));

    let finalDate = Date.now()
    let finalNumber = `${prefix}-${arr.join('')}${number} func time --->`

    return [finalNumber, finalDate-startDate, rest]
}
const [invoiceNum, performance, rest] = (someFunc(someInfo));
console.log(invoiceNum, performance, rest);