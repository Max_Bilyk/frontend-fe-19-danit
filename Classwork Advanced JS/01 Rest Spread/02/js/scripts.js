const jsonFromServer = {
    name__from_server: 'Max',
    money: 10000,
    address: {
        city: 'Kiev',
        zip: '02152',
        country: 'UA',
    },
    method1: function () {
        return 'calculated value'
    }
}
const {
    name__from_server:name,
    some_magic_property:magic='default value',
    money:moneyForUI,
    method1:someFunc
} = jsonFromServer
console.log(name, moneyForUI, magic);
console.log(someFunc('hello'));