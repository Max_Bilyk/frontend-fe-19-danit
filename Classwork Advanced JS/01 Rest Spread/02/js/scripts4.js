// Obj rest/spread
const obj1 = {name: 'Max'}
const obj2 = {saldo: 10000}

// const resultES5 = Object.assign({}, obj1, obj2);
// console.log(resultES5);
const obj3 = {...obj1, ...obj2, obj1}
// console.log(obj3);

function doCalc(p1, p2) {
    p1.addNewProperty = 10;
    p2.addAnotherProperty = 110000;

    console.log('Param 2 in doCalc ->', p2)
}
// doCalc(obj1, {...obj2});

//Arr rest/spread
const arr1 = [1,2,3,4];
const arr2 = [1000,2000,3000,4000];

const result = [...arr1, ...arr2]
// console.log(result);

for (const [key, val] of document.getElementsByTagName('div')){

}
const obj = {
    name: 'Max',
    age: 14,
}