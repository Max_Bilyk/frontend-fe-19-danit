//Arrays
function getCalcValues(a, b){
    return [
        a + b,
        a - b,
        // undefined,
        a * b,
        a / b
    ]
}
const [sum, sub = 'Default value', ...others] = getCalcValues(39, 14)
// const result = getCalcValues(39, 14);
// const sum = result[0]
// const sub = result[1]
// const [sub, sum] = result

// console.log(sum, sub, others );

//Objects
const person = {
    name: 'Max',
    age: 14,
    address: {
        country: 'UA',
        city: 'Kiev',

    }
};
// const name = person.name
// const {
//     name: firstName,
//     age,
//     car = 'no car',
//     // address
//     address: {city: homeTown, country}
// } = person;

// const {name, ...info} = person
// console.log(name, info);
function logPerson({name: FirstName, age}) {
    // console.log(per.name + ' ' + per.age);
    console.log(`Name: ${FirstName}, Age: ${age}`)
}
logPerson(person)