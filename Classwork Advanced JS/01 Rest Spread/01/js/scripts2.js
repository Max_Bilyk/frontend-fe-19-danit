const citiesUA = ['Киев', 'Харьков', 'Полтава', 'Суммы', 'Кривой Рог']
const citiesEU = ['Берлин', 'Прага', 'Париж',]
//Spread Arrays
// console.log(...citiesUA);
// console.log(...citiesEU);

// const allCities = [...citiesUA, ...citiesEU];
// const allCities = citiesEU.concat(citiesUA);
// console.log(allCities);

//Spread Objects
const citiesUAWithPopulation = {
    Kiev: 30,
    Kharkiv: 14,
    Poltava: 9,
    Summy: 5,
}
const citiesEUWithPopulation = {
    Kiev: 26,
    Berlin: 10,
    Prada: 3,
    Paris: 2,
}
// console.log({...citiesUAWithPopulation, ...citiesEUWithPopulation});

//Practise
// const numbersArr = [5, 37, 42, 17];
// console.log(Math.max(5, 37, 42, 17));
// console.log(Math.max(...numbersArr))
// console.log(Math.max.apply(null, numbersArr));

// const divs = document.querySelectorAll('div');
// const nodes = [...divs]
// console.log(divs, Array.isArray(divs));
// console.log(nodes, Array.isArray(nodes));

//Rest
function sum (a, b, ...otherNumbers){
    // console.log(otherNumbers);
    return a + b + rest.reduce((a, i) => a + i, 0);
}

const numbers = [1,2,3,4,5]
//Spread!!
// console.log(sum(...numbers));

// const a = numbers[0]
// const b = numbers[1]
// const  [a,b, ...otherNumbers] = numbers
// console.log(a, b, otherNumbers);

const person = {
    name: 'Max',
    age: 14,
    city: 'Kiev',
    country: 'UA'
}
const {name, age, ...address} = person
console.log(name, age, address);